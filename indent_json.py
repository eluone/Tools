#!/usr/bin/python
"""JSON flat file to indented (pretty) files"""
# Author: Tim Cumming
# Created: 30/10/21
# Modified: 30/11/21

import json
from pathlib import Path
import argparse
import sys
import textwrap


def load_json(json_file):
    # Lets build a dictionary from the json file provided
    if json_file.exists():
        with open(json_file, 'r') as f:
            json_dict = json.load(f)
    else:
        print("[!!] Input File: %s Not Found" % json_file)
        sys.exit()

    return json_dict


def save_json(json_file, output_data, spaces):
    # Save the dictionary to file
    with open(json_file, 'w') as f:
        json.dump(output_data, f, indent=spaces)


class JsonIndent:
    def __init__(self, args):
        self.args = args
        if self.args.input is None:
            print("[!!] Input File Missing")
            sys.exit()
        print("Input File %s" % self.args.input)
        if self.args.output is None:
            print("[!!] Output File Missing")
            sys.exit()
        print("Output File %s" % self.args.output)

    def run(self):
        dictionary = {}
        json_input_file = Path(self.args.input)

        dictionary.update(load_json(json_input_file))

        json_output_file = Path(self.args.output)

        # Lets just check the user isn't going to overwrite something
        if Path.exists(json_output_file):
            print("[!!] Output file already exists, OVERWRITE? (yes/no)")
            user_input = input()
            if user_input == "yes" or user_input == "y":
                print("[**] Output saved to %s" % self.args.output)
                save_json(json_output_file, dictionary, self.args.spaces)
            else:
                print("[**] Exiting")
                sys.exit()
        else:
            print("[**] Output saved to %s" % self.args.output)
            save_json(json_output_file, dictionary, self.args.spaces)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='JSON flat file to indented (pretty) files',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=textwrap.dedent('''Example:
          indent_json.py -f flat_json.json -o pretty_json.json -i 4
          '''))
    parser.add_argument('-i', '--input', help='Input file')
    parser.add_argument('-o', '--output', help='Output file')
    parser.add_argument('-s', '--spaces', type=int, default=4, help='Indent spaces')
    args = parser.parse_args()

    ji = JsonIndent(args)
    ji.run()
